<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/categories', function () {
    return view('categories');
});


Route::group(['prefix' => 'backend'], function () {
    Voyager::routes();
});
Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/logout', [HomeController::class, 'index']);
Route::get('/posts', [HomeController::class, 'blogs']);
Route::get('/post/{slug}', [HomeController::class, 'blogDetail']);
Route::get('/contacts', function () {
    return view('contact');
});

Route::get('/post/category/{cate}', [HomeController::class, 'blogCategories']);

Route::group(['prefix' => 'auth'], function () {
});

Auth::routes();

