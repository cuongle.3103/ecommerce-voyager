<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TCG\Voyager\Models\Post;
use TCG\Voyager\Models\Category;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function blogs()
    {
        $posts = Post::orderBy('id', 'desc')->paginate(10);
        $categories = Category::all();
        $featured = Post::orderBy('id', 'desc')->where('featured', '=', '1')->paginate(5);
        return view('blog', compact('posts', 'categories','featured'));
    }

    public function blogDetail($slug)
    {
        $post = Post::where('slug', '=', $slug)->firstOrFail();
        $categories = Category::all();
        $featured = Post::orderBy('id', 'desc')->where('featured', '=', '1')->paginate(5);
        return view('blog-details', compact('post', 'categories','featured'));
    }

    public function blogCategories($cate)
    {
        $category = Category::where('slug', '=' , $cate)->get();
        $categories = Category::all();
        $posts = Post::orderBy('id', 'desc')->where('category_id', '=' , $category[0]->id)->paginate(5);
        $featured = Post::orderBy('id', 'desc')->where('featured', '=', '1')->paginate(5);
        return view('blog', compact('posts', 'category','categories','featured'));
    }
}
