const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css', [
        //
    ]);

mix.styles([
    "resources/css/bootstrap.min.css",
    "resources/css/owl.carousel.min.css",
    "resources/css/slicknav.css",
    "resources/css/flaticon.css",
    "resources/css/animate.min.css",
    "resources/css/price_rangs.css",
    "resources/css/magnific-popup.css",
    "resources/css/fontawesome-all.min.css",
    "resources/css/themify-icons.css",
    "resources/css/slick.css",
    "resources/css/nice-select.css",
    "resources/css/style.css",
], 'public/css/style.css').options({
    postCss: [
        require('postcss-discard-comments')({
            removeAll: true
        })
    ]
});

mix.scripts([
    "resources/js/vendor/modernizr-3.5.0.min.js",
"resources/js/vendor/jquery-1.12.4.min.js",
"resources/js/popper.min.js",
"resources/js/bootstrap.min.js",
"resources/js/owl.carousel.min.js",
"resources/js/slick.min.js",
"resources/js/jquery.slicknav.min.js",
"resources/js/wow.min.js",
"resources/js/jquery.magnific-popup.js",
"resources/js/jquery.nice-select.min.js",
"resources/js/jquery.counterup.min.js",
"resources/js/waypoints.min.js",
"resources/js/price_rangs.js",
"resources/js/contact.js",
"resources/js/jquery.form.js",
"resources/js/jquery.validate.min.js",
"resources/js/mail-script.js",
"resources/js/jquery.ajaxchimp.min.js",
"resources/js/plugins.js",
"resources/js/main.js",

], 'public/js/script.js');
