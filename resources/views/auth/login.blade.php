@extends('layouts.master')

@section('content')
<main class="login-bg">
    <div class="login-form-area">
       <div class="login-form">
          <div class="login-heading">
             <span>Login</span>
          </div>
          <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="input-box">
                <div class="single-input-fields">
                    <label>Email Address</label>
                    <input type="text" placeholder="Email address"  id="email" name="email" value="{{ old('email') }}">
                    @if (!empty($errors) && $errors->has('email'))
                    <div class="callout small alert text-center" id="emailHelpText">
                        <p>{{ $errors->first('email') }}</p>
                    </div>
                @endif
                </div>
                <div class="single-input-fields">
                    <label>Password</label>
                    <input type="password" placeholder="Enter Password" id="password" name="password" >
                    @if (!empty($errors) && $errors->has('password'))
                    <div class="callout small alert text-center" id="passwordHelpText">
                        <p>{{ $errors->first('password') }}</p>
                    </div>
                @endif

                </div>
                <div class="single-input-fields login-check">
                    <input type="checkbox" id="fruit1" name="remember" {{ old('remember') ? 'checked' : '' }}>
                    <label for="fruit1">Keep me logged in</label>
                    <a href="{{ route('password.request') }}" class="f-right">Forgot Password?</a>
                </div>
            </div>
            <div class="login-footer">
                <p>Don’t have an account? <a href="{{ route('register') }}">Sign Up</a> here</p>
                <button class="submit-btn3" type="submit">Login</button>
            </div>
          </form>
       </div>
    </div>
</main>
@endsection
