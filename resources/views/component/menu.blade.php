<ul>
    @foreach($items as $menu_item)

        <li>
            <a href="{{ $menu_item->link() }}">{{ $menu_item->title }}</a>
            @if(count($menu_item->children))
            <ul class="submenu">
            @foreach ($menu_item->children as $item)
                <a href="{{ $item->link() }}">{{ $item->title }}</a>
                @endforeach
            </ul>
            @endif
        </li>

    @endforeach
</ul>
