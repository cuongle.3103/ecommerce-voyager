<!doctype html>
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title>{{ setting('site.title') }}</title>
      <meta name="description" content="{{ setting('site.description') }}">
      <meta name="keywords" content="{{ setting('site.keywords') }}">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="shortcut icon" type="image/x-icon" href="/img/icon/favicon.png">
      <link rel="stylesheet" href="/css/style.css">
   </head>
   <body>
      {{-- <div id="preloader-active">
         <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
               <div class="preloader-circle"></div>
               <div class="preloader-img pere-text">
                  <img src="/img/icon/loder.png" alt="loder">
               </div>
            </div>
         </div>
      </div> --}}
      <header>
         <div class="header-area">
            <div class="header-top d-none d-sm-block">
               <div class="container">
                  <div class="row">
                     <div class="col-xl-12">
                        <div class="d-flex justify-content-between flex-wrap align-items-center">
                           <div class="header-info-left">
                              <ul>
                                 <li><a href="/">About Us</a></li>
                                 <li><a href="/">Privacy</a></li>
                                 <li><a href="/">FAQ</a></li>
                                 <li><a href="/">Careers</a></li>
                              </ul>
                           </div>
                           <div class="header-info-right d-flex">
                              <ul class="order-list">
                                 <li><a href="/">My Wishlist</a></li>
                                 <li><a href="/">Track Your Order</a></li>
                              </ul>
                              <ul class="header-social">
                                 <li><a href="/"><i class="fab fa-facebook"></i></a></li>
                                 <li> <a href="/"><i class="fab fa-instagram"></i></a></li>
                                 <li><a href="/"><i class="fab fa-twitter"></i></a></li>
                                 <li><a href="/"><i class="fab fa-linkedin-in"></i></a></li>
                                 <li> <a href="/"><i class="fab fa-youtube"></i></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="header-mid header-sticky">
               <div class="container">
                  <div class="menu-wrapper">
                     <div class="logo">
                        <a href="/"><img src="/img/logo/logo.png" alt=""></a>
                     </div>
                     <div class="main-menu d-none d-lg-block">
                        <nav>
                           <ul id="navigation">
                               {{menu('Frontend', 'component.menu')}}
                           </ul>
                        </nav>
                     </div>
                     <div class="header-right">
                        <ul class="align-items-center">
                           <li>
                              <div class="nav-search search-switch hearer_icon">
                                 <a id="search_1" href="javascript:void(0)">
                                 <span class="flaticon-search"></span>
                                 </a>
                              </div>
                           </li>
                        @guest
                           {{-- @if (Route::has('login')) --}}
                              <li> <a href="{{ route('login') }}"><span class="flaticon-user"></span></a></li>
                           {{-- @elseif (Route::has('register'))
                              <li> <a href="{{ route('register') }}"><span class="flaticon-user"></span></a></li> --}}
                           {{-- @endif --}}
                       @else
                           <li>
                              <a href="{{ route('logout') }}"
                              onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                              <span class="flaticon-user text-white"> {{ Auth::user()->name }}  </a>
                                     <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                         @csrf
                                     </form>
                           </li>
                       @endguest
                           
                           <li class="cart"><a href="cart.html"><span class="flaticon-shopping-cart"></span></a> </li>
                        </ul>
                     </div>
                  </div>
                  <div class="search_input" id="search_input_box">
                     <form class="search-inner d-flex justify-content-between ">
                        <input type="text" class="form-control" id="search_input" placeholder="Search Here">
                        <button type="submit" class="btn"></button>
                        <span class="ti-close" id="close_search" title="Close Search"></span>
                     </form>
                  </div>
                  <div class="col-12">
                     <div class="mobile_menu d-block d-lg-none"></div>
                  </div>
               </div>
            </div>
            <div class="header-bottom text-center">
               <p>Sale Up To 50% Biggest Discounts. Hurry! Limited Perriod Offer <a href="/" class="browse-btn">Shop Now</a></p>
            </div>
         </div>
      </header>

      @yield('content')

      <footer>
         <div class="footer-wrapper gray-bg">
            <div class="footer-area footer-padding">
               <section class="subscribe-area">
                  <div class="container">
                     <div class="row justify-content-between subscribe-padding">
                        <div class="col-xxl-3 col-xl-3 col-lg-4">
                           <div class="subscribe-caption">
                              <h3>Subscribe Newsletter</h3>
                              <p>Subscribe newsletter to get 5% on all products.</p>
                           </div>
                        </div>
                        <div class="col-xxl-5 col-xl-6 col-lg-7 col-md-9">
                           <div class="subscribe-caption">
                              <form action="/">
                                 <input type="text" placeholder="Enter your email">
                                 <button class="subscribe-btn">Subscribe</button>
                              </form>
                           </div>
                        </div>
                        <div class="col-xxl-2 col-xl-2 col-lg-4">
                           <div class="footer-social">
                              <a href="https://bit.ly/sai4ull"><i class="fab fa-facebook"></i></a>
                              <a href="/"><i class="fab fa-instagram"></i></a>
                              <a href="/"><i class="fab fa-youtube"></i></a>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
               <div class="container">
                  <div class="row justify-content-between">
                     <div class="col-xl-3 col-lg-3 col-md-6 col-sm-8">
                        <div class="single-footer-caption mb-50">
                           <div class="single-footer-caption mb-20">
                              <div class="footer-logo mb-35">
                                 <a href="/"><img src="/img/logo/logo2_footer.png" alt=""></a>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6">
                        <div class="single-footer-caption mb-50">
                           <div class="footer-tittle">
                              <h4>Shop Men</h4>
                              <ul>
                                 <li><a href="/">Clothing Fashion</a></li>
                                 <li><a href="/">Winter</a></li>
                                 <li><a href="/">Summer</a></li>
                                 <li><a href="/">Formal</a></li>
                                 <li><a href="/">Casual</a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6">
                        <div class="single-footer-caption mb-50">
                           <div class="footer-tittle">
                              <h4>Shop Women</h4>
                              <ul>
                                 <li><a href="/">Clothing Fashion</a></li>
                                 <li><a href="/">Winter</a></li>
                                 <li><a href="/">Summer</a></li>
                                 <li><a href="/">Formal</a></li>
                                 <li><a href="/">Casual</a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6">
                        <div class="single-footer-caption mb-50">
                           <div class="footer-tittle">
                              <h4>Baby Collection</h4>
                              <ul>
                                 <li><a href="/">Clothing Fashion</a></li>
                                 <li><a href="/">Winter</a></li>
                                 <li><a href="/">Summer</a></li>
                                 <li><a href="/">Formal</a></li>
                                 <li><a href="/">Casual</a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6">
                        <div class="single-footer-caption mb-50">
                           <div class="footer-tittle">
                              <h4>Quick Links</h4>
                              <ul>
                                 <li><a href="/">Track Your Order</a></li>
                                 <li><a href="/">Support</a></li>
                                 <li><a href="/">FAQ</a></li>
                                 <li><a href="/">Carrier</a></li>
                                 <li><a href="/">About</a></li>
                                 <li><a href="/">Contact Us</a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="footer-bottom-area">
               <div class="container">
                  <div class="footer-border">
                     <div class="row">
                        <div class="col-xl-12 ">
                           <div class="footer-copy-right text-center">
                              <p>
                                 Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart color-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" rel="nofollow noopener">Colorlib</a>
                              </p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </footer>
      <div id="back-top">
         <a class="wrapper" title="Go to Top" href="/">
            <div class="arrows-container">
               <div class="arrow arrow-one"></div>
               <div class="arrow arrow-two"></div>
            </div>
         </a>
      </div>
      <script src="/js/script.js"></script>
   </body>
</html>
