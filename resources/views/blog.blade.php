@extends('layouts.master')
@section('content')
<main>
    <div class="hero-area section-bg2">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="slider-area">
                        <div class="slider-height2 slider-bg4 d-flex align-items-center justify-content-center">
                            <div class="hero-caption hero-caption2">
                                <h2>Blog</h2>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb justify-content-center">
                                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                                        <li class="breadcrumb-item"><a href="/posts">Blog</a></li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="blog_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mb-5 mb-lg-0">
                    <div class="blog_left_sidebar">
                        @if (count($posts))
                            @foreach ($posts as $post)
                                <article class="blog_item">
                                    <div class="blog_item_img">
                                        <img class="card-img rounded-0" src="{{ Voyager::image( $post->image ) }}" alt="{{$post->title}}" />
                                        <a href="/post/{{$post->slug}}" class="blog_item_date">
                                            <h3>{{date('d',strtotime($post->created_at))}}</h3>
                                            <p>{{date('M',strtotime($post->created_at))}}</p>
                                        </a>
                                    </div>
                                    <div class="blog_details">
                                        <a class="d-inline-block" href="/post/{{$post->slug}}">
                                            <h2 class="blog-head" style="color: #2d2d2d;">{{$post->title}}</h2>
                                        </a>
                                        <p>{{ str_limit($post->excerpt, $limit = 200, $end = '...') }}</p>
                                        {{-- <ul class="blog-info-link"> --}}
                                                <a href="/post/{{$post->slug}}">View <i class="fa fa-angle-double-right"></i> </a>
                                            {{-- <li>
                                                <a href="/post/{{$post->slug}}"><i class="fa fa-comments"></i> 03 Comments</a>
                                            </li> --}}
                                        {{-- </ul> --}}
                                    </div>
                                </article>
                            @endforeach
                        @endif
                        <nav class="blog-pagination justify-content-center d-flex">
                            <ul class="pagination">
                                {!! $posts->links() !!}
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="blog_right_sidebar">
                        <aside class="single_sidebar_widget search_widget">
                            <form action="blog.html#">
                                <div class="form-group m-0">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search Keyword" />
                                        <div class="input-group-append d-flex">
                                            <button class="boxed-btn2" type="button">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </aside>
                        <aside class="single_sidebar_widget post_category_widget">
                            <h4 class="widget_title" style="color: #2d2d2d;">Category</h4>
                            <ul class="list cat-list">
                                <li>
                                    <a href="/posts" class="d-flex">
                                        <p>All</p>
                                        {{-- <p>(37)</p> --}}
                                    </a>
                                </li>
                                @if (count($categories))
                                    @foreach($categories as $category)
                                        <li>
                                            <a href="/post/category/{{ $category->slug }}" class="d-flex">
                                                <p>{{$category->name}}</p>
                                                {{-- <p>(37)</p> --}}
                                            </a>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </aside>
                        <aside class="single_sidebar_widget popular_post_widget">
                            <h3 class="widget_title" style="color: #2d2d2d;">Featured Post</h3>
                            @if (count($featured))
                                @foreach ($featured as $item)
                                    <div class="media post_item">
                                        <img style="height:60px" src="{{ Voyager::image( $item->image ) }}" alt="{{$item->title}}" />
                                        <div class="media-body">
                                            <a href="/post/{{$item->slug}}">
                                                <h3 style="color: #2d2d2d;">{{ str_limit($item->excerpt, $limit = 20, $end = '...') }}</h3>
                                            </a>
                                            <p>{{date('D, M d, Y',strtotime($item->datetime))}}</p>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                          
                        </aside>
                        <aside class="single_sidebar_widget newsletter_widget">
                            <h4 class="widget_title" style="color: #2d2d2d;">Newsletter</h4>
                            <form action="blog.html#">
                                <div class="form-group">
                                    <input type="email" class="form-control" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email'" placeholder="Enter email" required />
                                </div>
                                <button class="button rounded-0 primary-bg text-white w-100 btn_1 boxed-btn" type="submit">Subscribe</button>
                            </form>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

@endsection
