@extends('layouts.master')
@section('content')
<main>
    <div class="hero-area section-bg2">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="slider-area">
                        <div class="slider-height2 slider-bg4 d-flex align-items-center justify-content-center">
                            <div class="hero-caption hero-caption2">
                                <h2>Blog Details</h2>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb justify-content-center">
                                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                                        <li class="breadcrumb-item"><a href="/post/{{$post->slug}}">Blog Details</a></li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="blog_area single-post-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 posts-list">
                    <div class="single-post">
                        <div class="feature-img">
                            <img class="img-fluid" src="{{ Voyager::image( $post->image ) }}" alt="{{$post->title}}"  />
                        </div>
                        <div class="blog_details">
                            <h2 style="color: #2d2d2d;">{{$post->title}}</h2>
                            <i class="my-3">{{date('D, M d, Y',strtotime($post->datetime))}}</i>
                            {{-- <ul class="blog-info-link mt-3 mb-4">
                                <li>
                                    <a href="blog-details.html#"><i class="fa fa-user"></i> Travel, Lifestyle</a>
                                </li>
                                <li>
                                    <a href="blog-details.html#"><i class="fa fa-comments"></i> 03 Comments</a>
                                </li>
                            </ul> --}}
                            <div class="mt-3">
                                {!!$post->body!!}
                            </div>
                        </div>
                    </div>
                    {{-- <div class="navigation-top">
                        <div class="d-sm-flex justify-content-between text-center">
                            <p class="like-info">
                                <span class="align-middle"><i class="fa fa-heart"></i></span> Lily and 4 people like this
                            </p>
                            <div class="col-sm-4 text-center my-2 my-sm-0"></div>
                            <ul class="social-icons">
                                <li>
                                    <a href="https://www.facebook.com/sai4ull"><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li>
                                    <a href="blog-details.html#"><i class="fab fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="blog-details.html#"><i class="fab fa-dribbble"></i></a>
                                </li>
                                <li>
                                    <a href="blog-details.html#"><i class="fab fa-behance"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="navigation-area">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-12 nav-left flex-row d-flex justify-content-start align-items-center">
                                    <div class="thumb">
                                        <a href="blog-details.html#">
                                            <img class="img-fluid" src="assets/img/post/preview.jpg" alt="" />
                                        </a>
                                    </div>
                                    <div class="arrow">
                                        <a href="blog-details.html#">
                                            <span class="lnr text-white ti-arrow-left"></span>
                                        </a>
                                    </div>
                                    <div class="detials">
                                        <p>Prev Post</p>
                                        <a href="blog-details.html#">
                                            <h4 style="color: #2d2d2d;">Space The Final Frontier</h4>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-12 nav-right flex-row d-flex justify-content-end align-items-center">
                                    <div class="detials">
                                        <p>Next Post</p>
                                        <a href="blog-details.html#">
                                            <h4 style="color: #2d2d2d;">Telescopes 101</h4>
                                        </a>
                                    </div>
                                    <div class="arrow">
                                        <a href="blog-details.html#">
                                            <span class="lnr text-white ti-arrow-right"></span>
                                        </a>
                                    </div>
                                    <div class="thumb">
                                        <a href="blog-details.html#">
                                            <img class="img-fluid" src="assets/img/post/next.jpg" alt="" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="blog-author">
                        <div class="media align-items-center">
                            <img src="assets/img/blog/author.png" alt="" />
                            <div class="media-body">
                                <a href="blog-details.html#">
                                    <h4>Harvard milan</h4>
                                </a>
                                <p>Second divided from form fish beast made. Every of seas all gathered use saying you're, he our dominion twon Second divided from</p>
                            </div>
                        </div>
                    </div>
                    <div class="comments-area">
                        <h4>05 Comments</h4>
                        <div class="comment-list">
                            <div class="single-comment justify-content-between d-flex">
                                <div class="user justify-content-between d-flex">
                                    <div class="thumb">
                                        <img src="assets/img/blog/comment_1.png" alt="" />
                                    </div>
                                    <div class="desc">
                                        <p class="comment">
                                            Multiply sea night grass fourth day sea lesser rule open subdue female fill which them Blessed, give fill lesser bearing multiply sea night grass fourth day sea lesser
                                        </p>
                                        <div class="d-flex justify-content-between">
                                            <div class="d-flex align-items-center">
                                                <h5>
                                                    <a href="blog-details.html#">Emilly Blunt</a>
                                                </h5>
                                                <p class="date">December 4, 2017 at 3:12 pm</p>
                                            </div>
                                            <div class="reply-btn">
                                                <a href="blog-details.html#" class="btn-reply text-uppercase">reply</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="comment-list">
                            <div class="single-comment justify-content-between d-flex">
                                <div class="user justify-content-between d-flex">
                                    <div class="thumb">
                                        <img src="assets/img/blog/comment_2.png" alt="" />
                                    </div>
                                    <div class="desc">
                                        <p class="comment">
                                            Multiply sea night grass fourth day sea lesser rule open subdue female fill which them Blessed, give fill lesser bearing multiply sea night grass fourth day sea lesser
                                        </p>
                                        <div class="d-flex justify-content-between">
                                            <div class="d-flex align-items-center">
                                                <h5>
                                                    <a href="blog-details.html#">Emilly Blunt</a>
                                                </h5>
                                                <p class="date">December 4, 2017 at 3:12 pm</p>
                                            </div>
                                            <div class="reply-btn">
                                                <a href="blog-details.html#" class="btn-reply text-uppercase">reply</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="comment-list">
                            <div class="single-comment justify-content-between d-flex">
                                <div class="user justify-content-between d-flex">
                                    <div class="thumb">
                                        <img src="assets/img/blog/comment_3.png" alt="" />
                                    </div>
                                    <div class="desc">
                                        <p class="comment">
                                            Multiply sea night grass fourth day sea lesser rule open subdue female fill which them Blessed, give fill lesser bearing multiply sea night grass fourth day sea lesser
                                        </p>
                                        <div class="d-flex justify-content-between">
                                            <div class="d-flex align-items-center">
                                                <h5>
                                                    <a href="blog-details.html#">Emilly Blunt</a>
                                                </h5>
                                                <p class="date">December 4, 2017 at 3:12 pm</p>
                                            </div>
                                            <div class="reply-btn">
                                                <a href="blog-details.html#" class="btn-reply text-uppercase">reply</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="comment-form">
                        <h4>Leave a Reply</h4>
                        <form class="form-contact comment_form" action="blog-details.html#" id="commentForm">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <textarea class="form-control w-100" name="comment" id="comment" cols="30" rows="9" placeholder="Write Comment"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" name="name" id="name" type="text" placeholder="Name" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" name="email" id="email" type="email" placeholder="Email" />
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <input class="form-control" name="website" id="website" type="text" placeholder="Website" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="button button-contactForm btn_1 boxed-btn">Post Comment</button>
                            </div>
                        </form>
                    </div> --}}
                </div>
                <div class="col-lg-4">
                    <div class="blog_right_sidebar">
                        <aside class="single_sidebar_widget search_widget">
                            <form action="blog-details.html#">
                                <div class="form-group m-0">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search Keyword" />
                                        <div class="input-group-append d-flex">
                                            <button class="boxed-btn2" type="button">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </aside>
                        <aside class="single_sidebar_widget post_category_widget">
                            <h4 class="widget_title" style="color: #2d2d2d;">Category</h4>
                            <ul class="list cat-list">
                                <li>
                                    <a href="/posts" class="d-flex">
                                        <p>All</p>
                                        {{-- <p>(37)</p> --}}
                                    </a>
                                </li>
                                @if (count($categories))
                                    @foreach($categories as $category)
                                        <li>
                                            <a href="/post/category/{{ $category->slug }}" class="d-flex">
                                                <p>{{$category->name}}</p>
                                                {{-- <p>(37)</p> --}}
                                            </a>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </aside>
                        <aside class="single_sidebar_widget popular_post_widget">
                            <h3 class="widget_title" style="color: #2d2d2d;">Featured Post</h3>
                            @if (count($featured))
                            @foreach ($featured as $item)
                                <div class="media post_item">
                                    <img style="height:60px" src="{{ Voyager::image( $item->image ) }}" alt="{{$item->title}}" />
                                    <div class="media-body">
                                        <a href="/post/{{$item->slug}}">
                                            <h3 style="color: #2d2d2d;">{{ str_limit($item->excerpt, $limit = 20, $end = '...') }}</h3>
                                        </a>
                                        <p>{{date('D, M d, Y',strtotime($item->datetime))}}</p>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        </aside>
                        {{-- <aside class="single_sidebar_widget tag_cloud_widget">
                            <h4 class="widget_title" style="color: #2d2d2d;">Tag Clouds</h4>
                            <ul class="list">
                                <li>
                                    <a href="blog-details.html#">project</a>
                                </li>
                                <li>
                                    <a href="blog-details.html#">love</a>
                                </li>
                                <li>
                                    <a href="blog-details.html#">technology</a>
                                </li>
                                <li>
                                    <a href="blog-details.html#">travel</a>
                                </li>
                                <li>
                                    <a href="blog-details.html#">restaurant</a>
                                </li>
                                <li>
                                    <a href="blog-details.html#">life style</a>
                                </li>
                                <li>
                                    <a href="blog-details.html#">design</a>
                                </li>
                                <li>
                                    <a href="blog-details.html#">illustration</a>
                                </li>
                            </ul>
                        </aside> --}}
                        <aside class="single_sidebar_widget newsletter_widget">
                            <h4 class="widget_title" style="color: #2d2d2d;">Newsletter</h4>
                            <form action="blog-details.html#">
                                <div class="form-group">
                                    <input type="email" class="form-control" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email'" placeholder="Enter email" required />
                                </div>
                                <button class="button rounded-0 primary-bg text-white w-100 btn_1 boxed-btn" type="submit">Subscribe</button>
                            </form>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

@endsection